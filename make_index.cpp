#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;

#define DEBUG 1

struct posting{
    long long docID;
    int frequency;
};

struct word_index{
    string term;
    long long start_pos;
    long long end_pos;
};

word_index write_word_to_final_index(ofstream* out, string term, vector<posting> single_term) {

    if(DEBUG)
        cout<<"Writing one word to file " << term <<endl;

    long long start_pos = out->tellp();

    string termstr = term + ":";

    cout<<single_term.size();

    for(long long i=0;i<single_term.size();i++) {
        termstr = termstr + to_string(single_term[i].docID) + "," + to_string(single_term[i].frequency) + ";";
    }

    termstr = termstr + "\n";

    *out << termstr;

    long long end_pos = out->tellp();

    word_index w;
    w.term = term;
    w.start_pos = start_pos;
    w.end_pos = end_pos;

    cout<<"Written";

    return w;
}

void write_wordlist_to_file(ofstream *word_file, vector<word_index> word_list) {

    if(DEBUG)
        cout<<"Writing word file" << endl;

    for(long long i=0;i<word_list.size();i++){
        *word_file << word_list[i].term << " " << word_list[i].start_pos << " " << word_list[i].end_pos << "\n";
    }
}

int main() {

    ifstream in ("temp_inverted_index", ios::in | ios::binary);

    string current_word;
    string line;
    vector<posting> single_term;
    vector<word_index> word_list;

    ofstream out ("inverted_index", ios::out | ios::binary);
    ofstream word_file ("word_index", ios::out | ios::binary);

    getline(in, line);
    stringstream ss;
    ss.str(line);
    long long docID;
    int frequency;
    string term;

    while(ss >> term) {
        ss >> docID;
        ss >>frequency;
    }

    current_word = term;
    posting p;
    p.docID = docID;
    p.frequency = frequency;

    single_term.push_back(p);

    while(getline(in, line)) {
        ss.clear();
        ss.str(line);
        while(ss >> term) {
            ss >> p.docID;
            ss >> p.frequency;
        }

        if(term == current_word) {
            single_term.push_back(p);
        }
        else {

            out << current_word << ":";
            long long start_pos = out.tellp();
            start_pos = start_pos + 1;

            for(long long i=0;i<single_term.size();i++){
               string str = to_string(single_term[i].docID) + "," + to_string(single_term[i].frequency) + ";";
               out << str;
            }

            out << "\n";

            long long end_pos = out.tellp();

            word_file << current_word << " " <<start_pos << " " << end_pos << " " << single_term.size() << "\n";

            single_term.clear();
            current_word = term;
            single_term.push_back(p);
        }
    }
    
    out << current_word << ":";
    long long start_pos = out.tellp();

    for(long long i=0;i<single_term.size();i++){
        string str = to_string(single_term[i].docID) + "," + to_string(single_term[i].frequency) + ";";
        out << str;
    }

    out << "\n";

    long long end_pos = out.tellp();
    start_pos = start_pos + 1;

    word_file << current_word << " " << start_pos << " " << end_pos << " " << single_term.size() << "\n";

    in.close();
    out.close();
    word_file.close();

    return 0;
}

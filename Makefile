CC = g++
CFLAGS = -lz

all: indexer make_index

indexer: indexer.cpp indexer.o
	$(CC) indexer.cpp -o indexer $(CFLAGS)

make_index: make_index.cpp make_index.o
	$(CC) make_index.cpp -o make_index

cleanup:
	rm -f *.o
	rm -f make_index
	rm -rf make_index.dSYM
	rm -f indexer
	rm -rf indexer.dSYM

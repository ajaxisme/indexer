#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <map>

#include "zlib.h"

#define PTAG_B	1
#define PTAG_I	2
#define	PTAG_H	3
#define PTAG_TITLE	4
#define PTAG_SCRIPT	5

#define _TITLE_TAG	0x0001
#define _B_TAG		0x0004
#define _H_TAG		0x0008
#define _I_TAG		0x0010

#define xl_isdigit(c) (((c) >= '0') && ((c) <= '9'))
#define xl_islower(c) (((c) >= 'a') && ((c) <= 'z'))
#define xl_isupper(c) (((c) >= 'A') && ((c) <= 'Z'))
#define xl_isindexable(c) (xl_isdigit(c) || xl_islower(c) || xl_isupper(c))
#define xl_tolower(c) ((c) += 'a' - 'A')

#define MAX_MEMORY 536870912 //512 MB
#define DEBUG 1

using namespace std;

struct posting{
    long long docID;
    string term;
    char tag;
    int frequency;
    //int pos;
};

struct lexicon_list {
    string term;
    int termID; //optional
    int file_pos; //pointer to position in inverted list
};

char* parser_init(char* doc)
{
	char *p;

	if (strncasecmp(doc, "HTTP/", 5))
		return NULL;
	
	for (p = doc; (*p != ' ')&&(*p); p++);
	if (*p == '\0')
		return NULL;

	if (atoi(p) != 200)
		return NULL;

	p = strstr(p,  "\r\n\r\n");
	if (p == NULL)
		return NULL;

	return p+4;
}

int tag_parser(char* tag, int len, char* back_tag)
{
	int i = 0;

	if (tag[0] == '/')
	{
		*back_tag = 1;
		i++;

	} else
		*back_tag = 0;

	switch (tag[i])
	{
	case 'b':
	case 'B':
	case 'i':
	case 'I':
		if (!isspace(tag[i+1]))
			return 0;
		if ((tag[i] == 'b') || (tag[i] == 'B'))
			return PTAG_B;
		return PTAG_I;

	case 'e':
	case 'E':
		i++;
		if (((tag[i]=='m')||(tag[i]=='M')) && (isspace(tag[i+1])))
			return PTAG_I;
		return 0;
	
	case 'h':
	case 'H':
		i++;
		if (((tag[i]>='1')&&(tag[i]<='6')) && (isspace(tag[i+1])))
			return PTAG_H;
		return 0;
	
	case 't':
	case 'T':
		i++;
		if ((0==strncasecmp(tag+i, "itle", 4)) && (isspace(tag[i+4])))
			return PTAG_TITLE;
		return 0;
	
	case 's':
	case 'S':
		i++;
		if ((0==strncasecmp(tag+i, "trong", 5)) && (isspace(tag[i+5])))
			return PTAG_B;
		if ((0==strncasecmp(tag+i, "cript", 5)) && (isspace(tag[i+5])))
			return PTAG_SCRIPT;
		return 0;

	default:
		break;
	}

	return 0;
}

#define xlbit_set(__b1, __b2)	((__b1) |= (__b2))
#define xlbit_unset(__b1, __b2)	((__b1) &= ~(__b2))
#define xlbit_check(__b1, __b2) ((__b1)&(__b2))

int parser(char* url, char* doc, char* buf, int blen)
{
	char *p, *purl, *word, *ptag, *pbuf;
	char ch, back_tag, intag, inscript;
	unsigned tag_flag;
	int ret;
        int maxlen = (blen-1)/2;

	p = parser_init(doc);
	if (p == NULL){
		return 0;
        }
	pbuf = buf;

/* parsing URL */
        /*
	purl = url;
	while (*purl != '\0')
	{
		if (!xl_isindexable(*purl))
		{
			purl++;
			continue;
		}

		word = purl;
		while (xl_isindexable(*purl))
		{
			if (xl_isupper(*purl))
				xl_tolower(*purl);
			purl++;
		}

		ch = *purl;
		*purl = '\0';

		if (pbuf-buf+purl-word+3 > blen-1)
			return -1;
		sprintf(pbuf, "%s U\n", word);
		pbuf += (purl-word)+3;

		*purl = ch;
	}
        */

/* parsing page */
	tag_flag = 0;
	intag = 0;
	inscript = 0;
        //int pos = 1;

	while ((p-doc) < maxlen)
	{
		if (!xl_isindexable(*p))
		{
			if (*p != '>')
			{
				if (*p == '<')
				{
					ptag = p;
					intag = 1;
				}
				p++;
				continue;
			}

			*p = ' ';
			ret = tag_parser(ptag+1, p-ptag, &back_tag);
			switch (ret)
			{
				case PTAG_B:

					if (back_tag == 0)
						xlbit_set(tag_flag, _B_TAG);
					else
						xlbit_unset(tag_flag, _B_TAG);
					break;

				case PTAG_I:

					if (back_tag == 0)
						xlbit_set(tag_flag, _I_TAG);
					else
						xlbit_unset(tag_flag, _I_TAG);
					break;

				case PTAG_H:

				if (back_tag == 0)
					xlbit_set(tag_flag, _H_TAG);
				else
					xlbit_unset(tag_flag, _H_TAG);
				break;

				case PTAG_TITLE:

					if (back_tag == 0)
						xlbit_set(tag_flag, _TITLE_TAG);
					else
						xlbit_unset(tag_flag, _TITLE_TAG);
					break;

				case PTAG_SCRIPT:

					if (back_tag == 0)
						inscript = 1;
					else
						inscript = 0;

				default:
					break;
			}

			intag = 0;
			p++;
			continue;
		}

		if (inscript || intag)
		{
			p++;
			continue;
		}

		word = p;
		while (xl_isindexable(*p))
		{
			if (xl_isupper(*p))
				xl_tolower(*p);
			p++;
		}

		ch = *p;
		*p = '\0';

		if (pbuf-buf+p-word+1 > blen-1)
			return -1;

		sprintf(pbuf, "%s ", word);
		pbuf += (p-word)+1;

		if (xlbit_check(tag_flag, _B_TAG))
		{
			if (pbuf-buf+1> blen-1)
				return -1;
			*pbuf = 'B';
			pbuf++;
		}

		if (xlbit_check(tag_flag, _H_TAG))
		{
			if (pbuf-buf+1> blen-1)
				return -1;
			*pbuf = 'H';
			pbuf++;
		}

		if (xlbit_check(tag_flag, _I_TAG))
		{
			if (pbuf-buf+1> blen-1)
				return -1;
			*pbuf = 'I';
			pbuf++;
		}

		if (xlbit_check(tag_flag, _TITLE_TAG))
		{
			if (pbuf-buf+1> blen-1)
				return -1;
			*pbuf = 'T';
			pbuf++;
		}

		if (tag_flag == 0)
		{
			if (pbuf-buf+1> blen-1)
				return -1;
			*pbuf = 'P';
			pbuf++;
		}

		if (pbuf-buf+1> blen-1)
			return -1;

                
                /*
                // Adding position imformation as well
                char* charpos = (char*) malloc (7*sizeof(char*));
                sprintf(charpos, " %d", pos++);

                for(int i=0;charpos[i];i++) {
                    *pbuf = charpos[i];
                    pbuf++;
                }
                free(charpos);
                */

		*pbuf = '\n';
		pbuf++;
		*p = ch;

	}

	*pbuf = '\0';
	return pbuf-buf;
}

/* Function to open a gzipped file and Return the file pointer */
gzFile openGZFile(string filename) {
    // Open a gzipped file and return file pointer
    //
    gzFile gzPtr = gzopen(filename.c_str(), "rb");
    if(gzPtr == NULL){
        cout<<"Error opening file: "<<filename<<endl;
        return NULL;
    }

    return gzPtr;
}

/* Function to read an entire zipped file and return a string containing contents */
string readEntireGzippedFile(string filename) {
    // Return entire gzipped index file as string(unzipped)

    gzFile gzPtr = openGZFile(filename);

    unsigned char buffer[8192];
    unsigned int unzipped_bytes;
    vector<unsigned char> unzippedData;

    while(true) {
        unzipped_bytes = gzread(gzPtr, buffer, 8192);
        if (unzipped_bytes > 0) {
                for(int i=0;i<unzipped_bytes;i++)
                    unzippedData.push_back(buffer[i]);
        }
        else
            break;
    }

    string content(unzippedData.begin(), unzippedData.end());

    gzclose(gzPtr);

    return content;
}

/* Function to read a chunk of opened gzipped file and return contents as string */
string readChunk(gzFile gzPtr, string filename, int len) {
    // Return one document as string (unzipped format)

    if (gzPtr == NULL) {
        gzPtr = openGZFile(filename);
        // Check for error in opening file
    }

    unsigned char* buffer = (unsigned char*) malloc (len*(sizeof(unsigned char*)));
    unsigned int unzipped_bytes;
    vector<unsigned char> unzippedData;

    unzipped_bytes = gzread(gzPtr, buffer, len);
    for(int i=0;i<unzipped_bytes;i++){
        unzippedData.push_back(buffer[i]);
    }

    string chunk(unzippedData.begin(), unzippedData.end());

    free(buffer);

    return chunk;
}

/* Sort function for  postings first based on termID and then docID */
bool compare_postings(posting a, posting b) {
    // First compare based on term
    // If equal, compare based on docID
    //
    if(a.term == b.term) {
        if(a.docID == b.docID) {
            return a.frequency < b.frequency;
        }
        else
            return a.docID < b.docID;
    }
    else
        return a.term < b.term;
}

/* Write intermediate postings list to disk */
void write_postings_to_disk(int tempID, vector<posting> postings) {
    string filename = to_string(tempID).append("_temp");
    ofstream temp_file (filename, ios::out | ios::binary);
    for(int i=0;i<postings.size();i++) {
        //string str = postings[i].term + " " + to_string(postings[i].docID) + " " + to_string(postings[i].pos) + "\n";
        string str = postings[i].term + " " + to_string(postings[i].docID) + " " + to_string(postings[i].frequency) + "\n";
        temp_file << str;
    }

    temp_file.close();
}

/* Main driver file to perform indexing */
int main() {

    /* For every index files, do the following
     * 1. Get the contents of the gzipped index file into memory
     * 2. Uncompress the contents in memory
     * 3. Open corresponding data file in gzipped format
     * 4. For every line in index file (corresponding to docID):
     *      1. Increment docID and assign that to URL
     *      2. Read contents till the content length mentioned
     *      3. Parsee the contents to extract all keywords for that particular doc
     *      4. Initialize DocLength to 0
     *      5. For every keyword:
     *          1. Get frequency and position
     *          2. Generate a posting as (docID, term, frequency, pos1, pos2)
     *          3. Write to a new file
     *          4. If keyword present, increase its frequency in term table
     *             else, add to the term table with frequency 1
     *          5. Increase DocLength by 1
     *      6. Write the (docID, url, IPAddress, DocLength) to another data strcuture
    */

    /* For all the listings, merge sort by (term, docID, frequency) */

    ofstream doc_ptr ("document_list", ios::out | ios::binary);

    int tempID = 0;
    long long docID = 0;
    int zipped_file_index = 0;
    vector<posting> postings;

    while(zipped_file_index < 2) {

        if(DEBUG)
            cout<<"Reading zipped file: "<<zipped_file_index<<endl;

        string index_file = to_string(zipped_file_index) + "_index";

        string data_file = to_string(zipped_file_index) + "_data";

        string index_data = readEntireGzippedFile(index_file);    

        vector<string> url_list; //Contains list of all sites crawled

        stringstream ss;
        ss.str(index_data);
        string line;
        while(getline(ss, line, '\n')) {
            url_list.push_back(line);
        }

        gzFile gzDataPtr = openGZFile(data_file);

        for(int i=0;i<url_list.size();i++) {

            stringstream ss1;
            int docSize = 0;

            map<string, posting> doc_postings;

            ss1.str(url_list[i]);
            string token;
            vector<string> tokens;

            while(getline(ss1, token, ' ')) {
                tokens.push_back(token);
            }

            // Values of interest (url, ip, content_length)
            string current_url = tokens[0];
            int content_length = stoi(tokens[3]);
            string current_ip = tokens[4];

            char* charurl = (char*) malloc (current_url.size()*sizeof(char*));

            for(int j=0;j<current_url.size();j++) {
                charurl[j] = current_url[j];
            }


            string htmlpage = readChunk(gzDataPtr, data_file, content_length);

            char* charhtmlpage = (char*) malloc (content_length*sizeof(char*));

            for(int j=0;j<htmlpage.size();j++) {
                charhtmlpage[j] = htmlpage[j];
            }

            int buffer_length = 2*content_length + 1;

            char* buffer = (char*) malloc (buffer_length*sizeof(char*));

            int tokens_length = parser(charurl, charhtmlpage, buffer, buffer_length);


            // Get parsed tokens as a string
            vector<char> parsed_tokens;

            for(int j=0;j<tokens_length;j++) {
                parsed_tokens.push_back(buffer[j]);
            }

            free(buffer);
            free(charhtmlpage);
            free(charurl);

            string tokens_string(parsed_tokens.begin(), parsed_tokens.end());

            // Extract tokens and make postings
            ss1.clear();
            ss1.str(tokens_string);
            string term_detail;
            while(getline(ss1, term_detail, '\n')) {
                stringstream ss2(term_detail);
                string term;
                char tag;
                while(ss2 >> term) {
                    ss2 >> tag;
                    //ss2 >> p.pos;
                }

                if(doc_postings.find(term) != doc_postings.end()) {
                    // term seen
                    doc_postings[term].frequency++;
                }
                else {
                    posting p;
                    p.docID = docID;
                    p.term = term;
                    p.tag = tag;
                    p.frequency = 1;

                    doc_postings[term] = p;
                }
                docSize++;

            }

            string docstring = to_string(docID) + " " + current_url + " " + current_ip + " " + to_string(docSize) + "\n";
            doc_ptr << docstring;

            for(map<string, posting>::iterator it=doc_postings.begin();it != doc_postings.end(); it++) {
                postings.push_back(it->second);

                // Check for memory size
                if( postings.size()*sizeof(posting) > MAX_MEMORY) {

                    if(DEBUG)
                        cout<<"Memory full, writing to disk....\n";
                    // Sort the postings
                    stable_sort(postings.begin(), postings.end(), compare_postings);
                    // Write to disk
                    write_postings_to_disk(tempID++, postings);
                     
                    // Clear postings
                    postings.clear();
                }
            }

            doc_postings.clear();

            docID++;
        }

        zipped_file_index++;
    }

    //Write last list of postings to file
    if(DEBUG)
        cout<<"Writing last file for a zipped file\n";

    stable_sort(postings.begin(), postings.end(), compare_postings);
    write_postings_to_disk(tempID++, postings);

    doc_ptr.close();


    cout<<endl;
    return 0;
}
